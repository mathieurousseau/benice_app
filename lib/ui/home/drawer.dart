import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class BeniceDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(padding: EdgeInsets.zero, children: <Widget>[
      DrawerHeader(
        child: Text('Drawer Header'),
        decoration: BoxDecoration(
          color: Colors.blue,
        ),
      ),
      ListTile(
        title: Text('French'),
        onTap: () {
          FlutterI18n.refresh(context, new Locale('fr'));
        },
      ),
      ListTile(
        title: Text('English'),
        onTap: () {
          FlutterI18n.refresh(context, new Locale('en'));
        },
      ),
    ]));
  }
}

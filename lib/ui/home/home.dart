import 'package:benice_app/ui/screens/consequences/consequences.dart';
import 'package:benice_app/ui/screens/consequences/consequences_home.dart';
import 'package:benice_app/ui/screens/dashboard/dashboard_home.dart';
import 'package:benice_app/ui/screens/rewards/rewards_home.dart';
import 'package:benice_app/ui/screens/tasks/tasks_home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _selectedIndex = 1;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  List<Widget> getWidgetOptions() {
    return [
      DashboardHome(),
      TasksHome(),
      ConsequencesHome(),
      RewardsHome(),
      ConsequencesList(),
    ];
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called
    return Scaffold(
        body: Center(
          child: getWidgetOptions().elementAt(_selectedIndex),
        ),
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: _selectedIndex,
          selectedItemColor: Colors.tealAccent,
          backgroundColor: Colors.red,
          showUnselectedLabels: true,
          onTap: _onItemTapped,
          items: [
            BottomNavigationBarItem(
              icon: new Icon(Icons.home),
              title: new Text(FlutterI18n.translate(context, "label.home")),
              backgroundColor: Colors.blue,
            ),
            BottomNavigationBarItem(
              icon: new Icon(Icons.list),
              title: new Text(FlutterI18n.translate(context, "label.tasks")),
              backgroundColor: Colors.blue,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.do_not_disturb),
              title: Text(FlutterI18n.translate(context, 'label.consequences')),
              backgroundColor: Colors.blue,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.star),
              title: Text(FlutterI18n.translate(context, 'label.rewards')),
              backgroundColor: Colors.blue,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.history),
              title: Text('History'),
              backgroundColor: Colors.blue,
            )
          ],
        ));
  }
}

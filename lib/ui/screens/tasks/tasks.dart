// Copyright 2019 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:benice_app/models/task.dart';
import 'package:benice_app/ui/screens/tasks/task_form.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:benice_app/bloc/task_bloc.dart';

class MyTaskList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var taskBloc = Provider.of<TaskBloc>(context);
    return Scaffold(
        body: StreamBuilder(
      stream: taskBloc.tasks,
      initialData: [],
      builder: (context, snapshot) => ListView.builder(
          padding: const EdgeInsets.all(8),
          itemCount: snapshot.data.length,
          itemBuilder: (BuildContext context, int index) {
            return _MyTask(snapshot.data[index]);
            // return Container(
            //   height: 50,
            //   // color: Colors.primaries[snapshot.data.id % Colors.primaries.length],
            //   child: Center(child: Text('${snapshot.data[index].id} :: ${snapshot.data[index].name}')),
            // );
          }),
    ));
  }
}

class _MyTask extends StatelessWidget {
  final Task task;

  _MyTask(this.task);

  @override
  Widget build(BuildContext context) {
    var taskBloc = Provider.of<TaskBloc>(context);
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: LimitedBox(
          maxHeight: 48,
          child: Row(children: [
            Center(
                child: Text(
                    '${task.id} :: ${task.name} :: ${task.icon} :: ${task.points}')),
            FlatButton(
              onPressed: () => showModalBottomSheet(
                  context: context, builder: (context) => TaskForm(task)),
              child: Icon(Icons.edit),
            ),
            FlatButton(
              onPressed: () => taskBloc.delete.add(task),
              child: Icon(Icons.delete),
            )
          ])),
    );
  }
}

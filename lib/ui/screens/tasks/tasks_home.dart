import 'package:benice_app/ui/home/drawer.dart';
import 'package:benice_app/models/task.dart';
import 'package:benice_app/ui/screens/tasks/task_form.dart';
import 'package:benice_app/ui/screens/tasks/tasks.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/widgets/I18nText.dart';

class TasksHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: BeniceDrawer(),
        appBar: AppBar(
          title: I18nText('label.tasks'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
                showModalBottomSheet(
                    context: context,
                    builder: (context) => TaskForm(Task.named()));
              },
            )
          ],
        ),
        body: MyTaskList());
  }
}

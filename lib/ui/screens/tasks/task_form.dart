// Define a custom Form widget.
import 'package:benice_app/models/task.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:provider/provider.dart';
import 'package:benice_app/bloc/task_bloc.dart';

class TaskForm extends StatefulWidget {
  final Task _task;
  TaskForm(this._task);
  @override
  TaskFormState createState() {
    return TaskFormState(this._task);
  }
}

class TaskFormState extends State<TaskForm> {
  final _formKey = GlobalKey<FormBuilderState>();
  final _task;

  TaskFormState(this._task);

  @override
  Widget build(BuildContext context) {
    var taskBloc = Provider.of<TaskBloc>(context);
    // Build a Form widget using the _formKey created above.
    return FormBuilder(
      key: _formKey,
      initialValue: {
        'name': _task.name,
        'icon': _task.icon,
        'points': _task.points.toString(),
      },
      autovalidate: true,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          FormBuilderTextField(
            attribute: 'name',
            validators: [FormBuilderValidators.required()],
            valueTransformer: (name) {
              _task.name = name;
            },
          ),
          FormBuilderTextField(
            attribute: 'icon',
            validators: [FormBuilderValidators.required()],
            valueTransformer: (icon) {
              _task.icon = icon;
            },
          ),
          FormBuilderTextField(
            attribute: 'points',
            keyboardType: TextInputType.number,
            validators: [
              FormBuilderValidators.required(),
              FormBuilderValidators.numeric(),
            ],
            valueTransformer: (points) {
              _task.points = num.parse(points);
            },
          ),

          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: RaisedButton(
              onPressed: () {
                if (_formKey.currentState.saveAndValidate()) {
                  taskBloc.save.add(_task);
                  _formKey.currentState.reset();
                  Navigator.pop(context);
                }
              },
              child: Text('Submit'),
            ),
          ),
        ],
      ),
    );
  }
}

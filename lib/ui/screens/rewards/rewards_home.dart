import 'package:benice_app/ui/home/drawer.dart';
import 'package:benice_app/models/reward.dart';
import 'package:benice_app/ui/screens/rewards/reward_form.dart';
import 'package:benice_app/ui/screens/rewards/rewards.dart';
import 'package:flutter/material.dart';

class RewardsHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: BeniceDrawer(),
        appBar: AppBar(
          title: Text('Rewards'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
                showModalBottomSheet(
                    context: context,
                    builder: (context) => RewardForm(Reward.named(icon: '❓')));
              },
            )
          ],
        ),
        body: RewardsList());
  }
}

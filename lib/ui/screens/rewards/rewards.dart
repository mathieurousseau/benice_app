// Copyright 2019 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:benice_app/ui/screens/rewards/reward_form.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:benice_app/bloc/rewards_bloc.dart';
import 'package:benice_app/models/reward.dart';

class RewardsList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var rewardsBloc = Provider.of<RewardsBloc>(context);
    return Scaffold(
      body: StreamBuilder(
        stream: rewardsBloc.rewards,
        initialData: [],
        builder: (context, snapshot) => ListView.builder(
          padding: const EdgeInsets.all(8),
          itemCount: snapshot.data.length,
          itemBuilder: (BuildContext context, int index) {
            final Reward reward = snapshot.data[index];
            return Container(
              height: 50,
              // color: Colors.primaries[snapshot.data.id % Colors.primaries.length],
              child: Row(children: [
                  Center(
                      child: Text(
                          '${reward.id} :: ${reward.name} :: ${reward.icon} :: ${reward.cost}')),
                  FlatButton(
                    onPressed: () => showModalBottomSheet(
                        context: context,
                        builder: (context) => RewardForm(reward)),
                    child: Icon(Icons.edit),
                  ),
                  FlatButton(
                    onPressed: () => rewardsBloc.delete.add(reward),
                    child: Icon(Icons.delete),
                  )
                ])
            );
          }
        ),
    ));
  }
}
// Define a custom Form widget.
import 'package:benice_app/models/reward.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:benice_app/bloc/rewards_bloc.dart';

class RewardForm extends StatefulWidget {
  final Reward _reward;
  RewardForm(this._reward);
  @override
  RewardFormState createState() {
    return RewardFormState(_reward);
  }
}

// Define a corresponding State class.
// This class holds data related to the form.
class RewardFormState extends State<RewardForm> {
  final _formKey = GlobalKey<FormState>();

  final Reward _reward;
  RewardFormState(this._reward);

  @override
  Widget build(BuildContext context) {
    var rewardsBloc = Provider.of<RewardsBloc>(context);
    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
              initialValue: _reward.name,
              validator: (name) {
                if (name.isEmpty) {
                  return 'Please enter a name';
                }
                return null;
              },
              onSaved: (val) => setState(() => _reward.name = val)),
          TextFormField(
              initialValue: _reward.icon,
              validator: (icon) {
                if (icon.isEmpty) {
                  return 'Please enter an icon';
                }
                return null;
              },
              onSaved: (val) => setState(() => _reward.icon = val)),
          TextFormField(
              initialValue: _reward.cost.toString(),
              validator: (cost) {
                if (cost.isEmpty) {
                  return 'Please enter a cost';
                }
                return null;
              },
              onSaved: (val) => setState(() => _reward.cost = num.parse(val))),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: RaisedButton(
              onPressed: () {
                // Validate returns true if the form is valid, or false
                // otherwise.
                if (_formKey.currentState.validate()) {
                  _formKey.currentState.save();
                  // If the form is valid, display a Snackbar.
                  rewardsBloc.updateReward.add(_reward);
                  // Scaffold.of(context)
                  // .showSnackBar(SnackBar(content: Text('Processing Data')));
                  _formKey.currentState.reset();
                  Navigator.pop(context);
                }
              },
              child: Text('Submit'),
            ),
          ),
        ],
      ),
    );
  }
}

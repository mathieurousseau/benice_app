// Define a custom Form widget.
import 'package:benice_app/models/family.dart';
import 'package:benice_app/bloc/family_bloc.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FamilyForm extends StatefulWidget {
  @override
  FamilyFormState createState() {
    return FamilyFormState();
  }
}

// Define a corresponding State class.
// This class holds data related to the form.
class FamilyFormState extends State<FamilyForm> {
  final _formKey = GlobalKey<FormState>();

  FamilyFormState();

  @override
  Widget build(BuildContext context) {
    var familyBloc = Provider.of<FamilyBloc>(context);
    // Build a Form widget using the _formKey created above.
    return StreamBuilder(
        stream: familyBloc.family,
        builder: (BuildContext context, AsyncSnapshot<Family> snapshot) {
          final Family _family = snapshot.data;
          return Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextFormField(
                    initialValue: _family.name,
                    validator: (name) {
                      if (name.isEmpty) {
                        return 'Please enter a name';
                      }
                      return null;
                    },
                    onSaved: (val) => setState(() => _family.name = val)),
                TextFormField(
                    initialValue: _family.description,
                    validator: (description) {
                      if (description.isEmpty) {
                        return 'Please enter an icon';
                      }
                      return null;
                    },
                    onSaved: (val) =>
                        setState(() => _family.description = val)),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: RaisedButton(
                    onPressed: () {
                      // Validate returns true if the form is valid, or false
                      // otherwise.
                      if (_formKey.currentState.validate()) {
                        _formKey.currentState.save();
                        // If the form is valid, display a Snackbar.
                        familyBloc.updateFamily.add(_family);
                        // Scaffold.of(context)
                        // .showSnackBar(SnackBar(content: Text('Processing Data')));
                        _formKey.currentState.reset();
                        Navigator.pop(context);
                      }
                    },
                    child: Text('Submit'),
                  ),
                ),
              ],
            ),
          );
        });
  }
}

import 'package:benice_app/ui/home/drawer.dart';
import 'package:benice_app/ui/screens/dashboard/dashboard.dart';
import 'package:benice_app/ui/screens/dashboard/family_form.dart';
import 'package:flutter/material.dart';

class DashboardHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: BeniceDrawer(),
        appBar: AppBar(
          title: Text('My Family'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
                showModalBottomSheet(
                    context: context, builder: (context) => FamilyForm());
              },
            )
          ],
        ),
        body: Dashboard());
  }
}

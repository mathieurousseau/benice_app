// Copyright 2019 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:benice_app/bloc/consequences_bloc.dart';
import 'package:benice_app/models/consequence.dart';

class ConsequencesList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var consequencesBloc = Provider.of<ConsequencesBloc>(context);
    return Scaffold(
      body: StreamBuilder(
        stream: consequencesBloc.consequences,
        initialData: [],
        builder: (context, snapshot) => CustomScrollView(
        slivers: [
          SliverList(
            delegate: SliverChildBuilderDelegate(
                (context, index) => _ConsequenceList(snapshot.data, index)),
          ),
        ],
      ),
    ));
  }
}


class _ConsequenceList extends StatelessWidget {
  final List<Consequence> consequences2;
  final int index;


  _ConsequenceList(this.consequences2, this.index, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var consequence = consequences2[index];
    var textTheme = Theme.of(context).textTheme.headline6;

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: LimitedBox(
        maxHeight: 48,
        child: Row(
          children: [
            AspectRatio(
              aspectRatio: 1,
              child: Container(
                color: Colors.primaries[consequence.id % Colors.primaries.length],
              ),
            ),
            SizedBox(width: 24),
            Expanded(
              child: Text('${consequence.id} :: ${consequence.name} :: ${consequence.icon}', style: textTheme),
            ),
            SizedBox(width: 24),
          ],
        ),
      ),
    );
  }
}
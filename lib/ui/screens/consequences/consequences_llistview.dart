// Copyright 2019 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:benice_app/ui/common/ConfirmationDialog.dart';
import 'package:benice_app/ui/common/ListItem.dart';
import 'package:benice_app/ui/screens/consequences/consequence_form.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:benice_app/bloc/consequences_bloc.dart';

class ConsequencesListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var consequencesBloc = Provider.of<ConsequencesBloc>(context);
    return Scaffold(
        body: StreamBuilder(
      stream: consequencesBloc.consequences,
      initialData: [],
      builder: (context, snapshot) => ListView.builder(
          padding: const EdgeInsets.all(8),
          itemCount: snapshot.data.length,
          itemBuilder: (BuildContext context, int index) {
            final consequence = snapshot.data[index];
            return ListItem(item: consequence, actions: [
              FlatButton(
                onPressed: () => showModalBottomSheet(
                    context: context,
                    builder: (context) =>
                        ConsequenceForm(snapshot.data[index])),
                child: Icon(Icons.edit),
              ),
              FlatButton(
                // onPressed: () => consequencesBloc.delete.add(consequence),
                onPressed: () => showConfirmationDialog(
                        context, "dialog.confirmation.consequence.delete")
                    .then((val) {
                  if (val) {
                    consequencesBloc.delete.add(consequence);
                  }
                }),

                child: Icon(Icons.delete),
              )
            ]);
          }),
    ));
  }
}

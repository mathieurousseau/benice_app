// Define a custom Form widget.
import 'package:benice_app/models/consequence.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:benice_app/bloc/consequences_bloc.dart';

class ConsequenceForm extends StatefulWidget {
  final Consequence consequence;
  ConsequenceForm(this.consequence);
  @override
  ConsequenceFormState createState() {
    return ConsequenceFormState(consequence);
  }
}

// Define a corresponding State class.
// This class holds data related to the form.
class ConsequenceFormState extends State<ConsequenceForm> {
  final Consequence _consequence;
  ConsequenceFormState(this._consequence);
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a `GlobalKey<FormState>`,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    var consequencesBloc = Provider.of<ConsequencesBloc>(context);
    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
              initialValue: _consequence.name,
              validator: (name) {
                if (name.isEmpty) {
                  return 'Please enter a name';
                }
                return null;
              },
              onSaved: (val) => setState(() => _consequence.name = val)),
          TextFormField(
              initialValue: _consequence.icon,
              validator: (icon) {
                if (icon.isEmpty) {
                  return 'Please enter an icon';
                }
                return null;
              },
              onSaved: (val) => setState(() => _consequence.icon = val)),
          TextFormField(
              keyboardType: TextInputType.number,
              initialValue: _consequence.points.toString(),
              validator: (points) {
                if (points.isEmpty) {
                  return 'Please enter points';
                }
                return null;
              },
              onSaved: (val) => setState(() => _consequence.points = num.parse(val))),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: RaisedButton(
              onPressed: () {
                // Validate returns true if the form is valid, or false
                // otherwise.
                if (_formKey.currentState.validate()) {
                  _formKey.currentState.save();
                  // If the form is valid, display a Snackbar.
                  consequencesBloc.updateConsequence.add(_consequence);
                  // Scaffold.of(context)
                  // .showSnackBar(SnackBar(content: Text('Processing Data')));
                  _formKey.currentState.reset();
                  Navigator.pop(context);
                }
              },
              child: Text('Submit'),
            ),
          ),
        ],
      ),
    );
  }
}

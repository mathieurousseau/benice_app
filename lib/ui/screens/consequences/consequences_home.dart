import 'package:benice_app/ui/common/DrawerIcon.dart';
import 'package:benice_app/ui/home/drawer.dart';
import 'package:benice_app/models/consequence.dart';
import 'package:benice_app/ui/screens/consequences/consequence_form.dart';
import 'package:benice_app/ui/screens/consequences/consequences_llistview.dart';
import 'package:benice_app/bloc/consequences_bloc.dart';
import 'package:flutter/material.dart';
import 'package:kiwi/kiwi.dart';

class ConsequencesHome extends StatefulWidget {
  @override
  _ConsequencesHomeState createState() => _ConsequencesHomeState();
}

class _ConsequencesHomeState extends State<ConsequencesHome> {
  Icon _searchIcon = new Icon(Icons.search);
  Widget _appBarTitle = new Text('Consequences');
  ConsequencesBloc consequencesBloc =
      KiwiContainer().resolve<ConsequencesBloc>();
  final TextEditingController _filter = new TextEditingController();

  _ConsequencesHomeState() {
    _filter.addListener(() {
      if (_filter.text.isEmpty) {
        consequencesBloc.searchToken.add('%');
      } else {
        consequencesBloc.searchToken.add(_filter.text);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: BeniceDrawer(),
        appBar: AppBar(
          leading: DrawerIcon(),
          title: _appBarTitle,
          centerTitle: true,
          actions: <Widget>[
            IconButton(
              icon: this._searchIcon,
              onPressed: _searchPressed,
            ),
            IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
                showModalBottomSheet(
                    context: context,
                    builder: (context) => ConsequenceForm(Consequence.empty()));
              },
            ),
          ],
        ),
        body: ConsequencesListView());
  }

  void _searchPressed() {
    setState(() {
      if (this._searchIcon.icon == Icons.search) {
        this._searchIcon = new Icon(Icons.close);
        this._appBarTitle = new TextField(
          controller: _filter,
          decoration: new InputDecoration(
              prefixIcon: new Icon(Icons.search), hintText: 'Search...'),
        );
      } else {
        this._searchIcon = new Icon(Icons.search);
        this._appBarTitle = new Text('Consequences');
        consequencesBloc.searchToken.add('%');
      }
    });
  }
}

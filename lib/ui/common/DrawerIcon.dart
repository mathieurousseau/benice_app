import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DrawerIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Builder(builder: (BuildContext context) {
      return IconButton(
          icon: Icon(
            Icons.account_circle,
            color: Colors.white,
            size: 30.0,
          ),
          onPressed: () {
            Scaffold.of(context).openDrawer();
          });
    });
  }
}

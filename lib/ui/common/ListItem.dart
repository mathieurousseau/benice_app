import 'package:benice_app/models/entity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ListItem extends StatelessWidget {
  final ListEntity item;
  final List<Widget> actions;

  ListItem({
    Key key,
    this.item,
    this.actions,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 50,
        // color: Colors.primaries[snapshot.data.id % Colors.primaries.length],
        child: Row(children: [
          Center(
              child: Text(
                  'CLV: ${item.id} :-: ${item.name} :-: ${item.icon} :-: ${item.points}')),
          this.actions[0],
          this.actions[1],
        ]));
  }
}

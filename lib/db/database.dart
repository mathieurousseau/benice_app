import 'dart:async';
import 'package:benice_app/dao/consequence_dao.dart';
import 'package:benice_app/dao/family_dao.dart';
import 'package:benice_app/dao/reward_dao.dart';
import 'package:benice_app/dao/task_dao.dart';
import 'package:benice_app/models/consequence.dart';
import 'package:benice_app/models/family.dart';
import 'package:benice_app/models/reward.dart';
import 'package:benice_app/models/task.dart';
import 'package:floor/floor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;   


part 'database.g.dart'; // the generated code will be there

@Database(version: 1, entities: [Task, Consequence, Reward, Family])
abstract class AppDatabase extends FloorDatabase {
  TaskDao get taskDao;
  ConsequenceDao get consequenceDao;
  RewardDao get rewardDao;
  FamilyDao get familyDao;
}
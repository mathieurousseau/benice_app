// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

class $FloorAppDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder databaseBuilder(String name) =>
      _$AppDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$AppDatabaseBuilder(null);
}

class _$AppDatabaseBuilder {
  _$AppDatabaseBuilder(this.name);

  final String name;

  final List<Migration> _migrations = [];

  Callback _callback;

  /// Adds migrations to the builder.
  _$AppDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$AppDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<AppDatabase> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name)
        : ':memory:';
    final database = _$AppDatabase();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$AppDatabase extends AppDatabase {
  _$AppDatabase([StreamController<String> listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  TaskDao _taskDaoInstance;

  ConsequenceDao _consequenceDaoInstance;

  RewardDao _rewardDaoInstance;

  FamilyDao _familyDaoInstance;

  Future<sqflite.Database> open(String path, List<Migration> migrations,
      [Callback callback]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 1,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `tasks` (`id` INTEGER, `name` TEXT, `icon` TEXT, `points` INTEGER, `backEndId` INTEGER, PRIMARY KEY (`id`))');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `Consequences` (`name` TEXT NOT NULL, `icon` TEXT, `points` INTEGER, `id` INTEGER PRIMARY KEY AUTOINCREMENT, `backEndId` INTEGER)');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `Rewards` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `name` TEXT NOT NULL, `icon` TEXT, `cost` INTEGER, `backEndId` INTEGER)');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `Families` (`id` INTEGER, `name` TEXT NOT NULL, `description` TEXT, PRIMARY KEY (`id`))');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  TaskDao get taskDao {
    return _taskDaoInstance ??= _$TaskDao(database, changeListener);
  }

  @override
  ConsequenceDao get consequenceDao {
    return _consequenceDaoInstance ??=
        _$ConsequenceDao(database, changeListener);
  }

  @override
  RewardDao get rewardDao {
    return _rewardDaoInstance ??= _$RewardDao(database, changeListener);
  }

  @override
  FamilyDao get familyDao {
    return _familyDaoInstance ??= _$FamilyDao(database, changeListener);
  }
}

class _$TaskDao extends TaskDao {
  _$TaskDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database, changeListener),
        _taskInsertionAdapter = InsertionAdapter(
            database,
            'tasks',
            (Task item) => <String, dynamic>{
                  'id': item.id,
                  'name': item.name,
                  'icon': item.icon,
                  'points': item.points,
                  'backEndId': item.backEndId
                },
            changeListener),
        _taskUpdateAdapter = UpdateAdapter(
            database,
            'tasks',
            ['id'],
            (Task item) => <String, dynamic>{
                  'id': item.id,
                  'name': item.name,
                  'icon': item.icon,
                  'points': item.points,
                  'backEndId': item.backEndId
                },
            changeListener),
        _taskDeletionAdapter = DeletionAdapter(
            database,
            'tasks',
            ['id'],
            (Task item) => <String, dynamic>{
                  'id': item.id,
                  'name': item.name,
                  'icon': item.icon,
                  'points': item.points,
                  'backEndId': item.backEndId
                },
            changeListener);

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  static final _tasksMapper = (Map<String, dynamic> row) => Task(
      row['id'] as int,
      row['name'] as String,
      row['icon'] as String,
      row['points'] as int);

  final InsertionAdapter<Task> _taskInsertionAdapter;

  final UpdateAdapter<Task> _taskUpdateAdapter;

  final DeletionAdapter<Task> _taskDeletionAdapter;

  @override
  Future<List<Task>> findAllTasks() async {
    return _queryAdapter.queryList('SELECT * FROM Tasks', mapper: _tasksMapper);
  }

  @override
  Stream<List<Task>> findAllTasksAsStream() {
    return _queryAdapter.queryListStream('SELECT * FROM Tasks',
        queryableName: 'tasks', isView: false, mapper: _tasksMapper);
  }

  @override
  Stream<List<Task>> findTasks(String token) {
    return _queryAdapter.queryListStream(
        'SELECT * FROM Tasks WHERE name like ?',
        arguments: <dynamic>[token],
        queryableName: 'tasks',
        isView: false,
        mapper: _tasksMapper);
  }

  @override
  Future<Task> findTaskById(int id) async {
    return _queryAdapter.query('SELECT * FROM Tasks WHERE id = ?',
        arguments: <dynamic>[id], mapper: _tasksMapper);
  }

  @override
  Future<int> insertTask(Task task) {
    return _taskInsertionAdapter.insertAndReturnId(
        task, OnConflictStrategy.abort);
  }

  @override
  Future<void> updateTask(Task task) async {
    await _taskUpdateAdapter.update(task, OnConflictStrategy.abort);
  }

  @override
  Future<void> deleteTask(Task task) async {
    await _taskDeletionAdapter.delete(task);
  }
}

class _$ConsequenceDao extends ConsequenceDao {
  _$ConsequenceDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database, changeListener),
        _consequenceInsertionAdapter = InsertionAdapter(
            database,
            'Consequences',
            (Consequence item) => <String, dynamic>{
                  'name': item.name,
                  'icon': item.icon,
                  'points': item.points,
                  'id': item.id,
                  'backEndId': item.backEndId
                },
            changeListener),
        _consequenceUpdateAdapter = UpdateAdapter(
            database,
            'Consequences',
            ['id'],
            (Consequence item) => <String, dynamic>{
                  'name': item.name,
                  'icon': item.icon,
                  'points': item.points,
                  'id': item.id,
                  'backEndId': item.backEndId
                },
            changeListener),
        _consequenceDeletionAdapter = DeletionAdapter(
            database,
            'Consequences',
            ['id'],
            (Consequence item) => <String, dynamic>{
                  'name': item.name,
                  'icon': item.icon,
                  'points': item.points,
                  'id': item.id,
                  'backEndId': item.backEndId
                },
            changeListener);

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  static final _consequencesMapper = (Map<String, dynamic> row) => Consequence(
      row['id'] as int,
      row['name'] as String,
      row['icon'] as String,
      row['points'] as int);

  final InsertionAdapter<Consequence> _consequenceInsertionAdapter;

  final UpdateAdapter<Consequence> _consequenceUpdateAdapter;

  final DeletionAdapter<Consequence> _consequenceDeletionAdapter;

  @override
  Future<List<Consequence>> findAllConsequences() async {
    return _queryAdapter.queryList('SELECT * FROM Consequences',
        mapper: _consequencesMapper);
  }

  @override
  Stream<List<Consequence>> findAllConsequencesAsStream() {
    return _queryAdapter.queryListStream('SELECT * FROM Consequences',
        queryableName: 'Consequences',
        isView: false,
        mapper: _consequencesMapper);
  }

  @override
  Stream<List<Consequence>> findConsequences(String token) {
    return _queryAdapter.queryListStream(
        'SELECT * FROM Consequences WHERE name like ?',
        arguments: <dynamic>[token],
        queryableName: 'Consequences',
        isView: false,
        mapper: _consequencesMapper);
  }

  @override
  Future<Consequence> findConsequenceById(int id) async {
    return _queryAdapter.query('SELECT * FROM Consequences WHERE id = ?',
        arguments: <dynamic>[id], mapper: _consequencesMapper);
  }

  @override
  Future<int> insertConsequence(Consequence consequence) {
    return _consequenceInsertionAdapter.insertAndReturnId(
        consequence, OnConflictStrategy.abort);
  }

  @override
  Future<void> updateConsequence(Consequence consequence) async {
    await _consequenceUpdateAdapter.update(
        consequence, OnConflictStrategy.abort);
  }

  @override
  Future<void> deleteConsequence(Consequence consequence) async {
    await _consequenceDeletionAdapter.delete(consequence);
  }
}

class _$RewardDao extends RewardDao {
  _$RewardDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database, changeListener),
        _rewardInsertionAdapter = InsertionAdapter(
            database,
            'Rewards',
            (Reward item) => <String, dynamic>{
                  'id': item.id,
                  'name': item.name,
                  'icon': item.icon,
                  'cost': item.cost,
                  'backEndId': item.backEndId
                },
            changeListener),
        _rewardUpdateAdapter = UpdateAdapter(
            database,
            'Rewards',
            ['id'],
            (Reward item) => <String, dynamic>{
                  'id': item.id,
                  'name': item.name,
                  'icon': item.icon,
                  'cost': item.cost,
                  'backEndId': item.backEndId
                },
            changeListener),
        _rewardDeletionAdapter = DeletionAdapter(
            database,
            'Rewards',
            ['id'],
            (Reward item) => <String, dynamic>{
                  'id': item.id,
                  'name': item.name,
                  'icon': item.icon,
                  'cost': item.cost,
                  'backEndId': item.backEndId
                },
            changeListener);

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  static final _rewardsMapper = (Map<String, dynamic> row) => Reward(
      row['id'] as int,
      row['name'] as String,
      row['icon'] as String,
      row['cost'] as int);

  final InsertionAdapter<Reward> _rewardInsertionAdapter;

  final UpdateAdapter<Reward> _rewardUpdateAdapter;

  final DeletionAdapter<Reward> _rewardDeletionAdapter;

  @override
  Future<List<Reward>> findAllRewards() async {
    return _queryAdapter.queryList('SELECT * FROM Rewards',
        mapper: _rewardsMapper);
  }

  @override
  Stream<List<Reward>> findAllRewardsAsStream() {
    return _queryAdapter.queryListStream('SELECT * FROM Rewards',
        queryableName: 'Rewards', isView: false, mapper: _rewardsMapper);
  }

  @override
  Future<Reward> findRewardById(int id) async {
    return _queryAdapter.query('SELECT * FROM Rewards WHERE id = ?',
        arguments: <dynamic>[id], mapper: _rewardsMapper);
  }

  @override
  Future<int> insertReward(Reward reward) {
    return _rewardInsertionAdapter.insertAndReturnId(
        reward, OnConflictStrategy.abort);
  }

  @override
  Future<void> updateReward(Reward reward) async {
    await _rewardUpdateAdapter.update(reward, OnConflictStrategy.abort);
  }

  @override
  Future<void> deleteReward(Reward reward) async {
    await _rewardDeletionAdapter.delete(reward);
  }
}

class _$FamilyDao extends FamilyDao {
  _$FamilyDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database, changeListener),
        _familyInsertionAdapter = InsertionAdapter(
            database,
            'Families',
            (Family item) => <String, dynamic>{
                  'id': item.id,
                  'name': item.name,
                  'description': item.description
                },
            changeListener),
        _familyUpdateAdapter = UpdateAdapter(
            database,
            'Families',
            ['id'],
            (Family item) => <String, dynamic>{
                  'id': item.id,
                  'name': item.name,
                  'description': item.description
                },
            changeListener);

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  static final _familiesMapper = (Map<String, dynamic> row) => Family(
      row['id'] as int, row['name'] as String, row['description'] as String);

  final InsertionAdapter<Family> _familyInsertionAdapter;

  final UpdateAdapter<Family> _familyUpdateAdapter;

  @override
  Stream<Family> findFamily() {
    return _queryAdapter.queryStream('SELECT * FROM Families WHERE id = 1',
        queryableName: 'Families', isView: false, mapper: _familiesMapper);
  }

  @override
  Future<Family> findFamilyFuture() async {
    return _queryAdapter.query('SELECT * FROM Families WHERE id = 1',
        mapper: _familiesMapper);
  }

  @override
  Future<int> insertFamily(Family family) {
    return _familyInsertionAdapter.insertAndReturnId(
        family, OnConflictStrategy.abort);
  }

  @override
  Future<void> updateFamily(Family family) async {
    await _familyUpdateAdapter.update(family, OnConflictStrategy.abort);
  }
}

import 'package:benice_app/models/user.dart';
import 'package:floor/floor.dart';

@dao
abstract class UserDao {
  
  @Query('SELECT * FROM Users WHERE id = 1')
  Stream<User> findUser();

  @insert
  Future<int> insertUser(User user);

  @update
  Future<void> updateUser(User user);

}
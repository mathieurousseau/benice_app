import 'package:benice_app/models/family.dart';
import 'package:floor/floor.dart';

@dao
abstract class FamilyDao {
  
  @Query('SELECT * FROM Families WHERE id = 1')
  Stream<Family> findFamily();

  @Query('SELECT * FROM Families WHERE id = 1')
  Future<Family> findFamilyFuture();

  @insert
  Future<int> insertFamily(Family family);

  @update
  Future<void> updateFamily(Family family);

}
import 'package:benice_app/models/consequence.dart';
import 'package:floor/floor.dart';

@dao
abstract class ConsequenceDao {
  @Query('SELECT * FROM Consequences')
  Future<List<Consequence>> findAllConsequences();

  @Query('SELECT * FROM Consequences')
  Stream<List<Consequence>> findAllConsequencesAsStream();

  @Query('SELECT * FROM Consequences WHERE name like :token')
  Stream<List<Consequence>> findConsequences(String token);
  
  @Query('SELECT * FROM Consequences WHERE id = :id')
  Future<Consequence> findConsequenceById(int id);

  @insert
  Future<int> insertConsequence(Consequence consequence);

  @update
  Future<void> updateConsequence(Consequence consequence);

  @delete
  Future<void> deleteConsequence(Consequence consequence);
}
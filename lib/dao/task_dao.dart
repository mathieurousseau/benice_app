import 'package:benice_app/models/task.dart';
import 'package:floor/floor.dart';

@dao
abstract class TaskDao {
  @Query('SELECT * FROM Tasks')
  Future<List<Task>> findAllTasks();

  @Query('SELECT * FROM Tasks')
  Stream<List<Task>> findAllTasksAsStream();

  @Query('SELECT * FROM Tasks WHERE name like :token')
  Stream<List<Task>> findTasks(String token);
  
  @Query('SELECT * FROM Tasks WHERE id = :id')
  Future<Task> findTaskById(int id);
  
  @insert
  Future<int> insertTask(Task task);

  @update
  Future<void> updateTask(Task task);

  @delete
  Future<void> deleteTask(Task task);
}
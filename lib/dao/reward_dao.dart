import 'package:benice_app/models/reward.dart';
import 'package:floor/floor.dart';

@dao
abstract class RewardDao {
  @Query('SELECT * FROM Rewards')
  Future<List<Reward>> findAllRewards();

  @Query('SELECT * FROM Rewards')
  Stream<List<Reward>> findAllRewardsAsStream();
  
  @Query('SELECT * FROM Rewards WHERE id = :id')
  Future<Reward> findRewardById(int id);

  @insert
  Future<int> insertReward(Reward reward);

  @update
  Future<void> updateReward(Reward reward);

  @delete
  Future<void> deleteReward(Reward reward);
}
import 'package:floor/floor.dart';

@Entity(tableName: 'Rewards')
class Reward {
  @PrimaryKey(autoGenerate: true)
  
  final int id;

  @ColumnInfo(nullable: false)
  String name;

  String icon;

  int cost;

  int backEndId;


  Reward(this.id, this.name, [ this.icon = '❓', this.cost = 1 ]);

  Reward.named({ this.id, this.name, this.icon = '❓' , this.cost = 1});
}
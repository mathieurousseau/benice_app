import 'package:floor/floor.dart';

@Entity(tableName: 'users')
class User {
  @primaryKey
  final int id;
  String name;
  String description;

  User(this.id, this.name, this.description);
  User.named({this.id, this.name, this.description});
}
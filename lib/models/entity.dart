import 'package:floor/floor.dart';

abstract class BnEntity {
  @PrimaryKey(autoGenerate: true)  
  int id;
  int backEndId;
} 

abstract class ListEntity extends BnEntity {
  @ColumnInfo(nullable: false)
  String name;
  String icon;
  int points;
}
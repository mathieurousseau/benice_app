import 'package:floor/floor.dart';

@Entity(tableName: 'tasks')
class Task {
  @primaryKey
  final int id;
  String name;
  String icon;
  int points;
  int backEndId;

  Task(this.id, this.name, [this.icon = '❓', this.points = 1]);
  Task.named({this.id, this.name, this.icon = '❓', this.points = 1});
}
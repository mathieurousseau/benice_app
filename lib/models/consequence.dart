import 'package:floor/floor.dart';

import 'entity.dart';

@Entity(tableName: 'Consequences')
class Consequence extends ListEntity {
  Consequence(int id, String name, [String icon = '❓', int points = -1]) {
    this.id = id;
    this.name = name;
    this.icon = icon;
    this.points = points;
  }
  Consequence.named({int id, String name, String icon = '❓'}) {
    this.id = id;
    this.name = name;
    this.icon = icon;
  }
  Consequence.empty({int id, String icon = '❓', int points = -1}) {
    this.id = id;
    this.icon = icon;
    this.points = points;
  }
}

import 'package:floor/floor.dart';

@Entity(tableName: 'Families')
class Family {
  @primaryKey
  final int id;

  @ColumnInfo(nullable: false)
  String name;
  String description;

  Family(this.id, this.name, this.description);
  Family.named({this.id, this.name, this.description});

}

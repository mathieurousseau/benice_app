import 'package:benice_app/bloc/consequences_bloc.dart';
import 'package:benice_app/bloc/family_bloc.dart';
import 'package:benice_app/bloc/rewards_bloc.dart';
import 'package:benice_app/bloc/task_bloc.dart';
import 'package:flutter/material.dart';
import 'package:benice_app/ui/home/home.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_i18n/flutter_i18n_delegate.dart';
import 'package:flutter_i18n/loaders/decoders/yaml_decode_strategy.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:benice_app/db/database.dart';
import 'package:kiwi/kiwi.dart';
import 'package:sqflite/sqflite.dart';

Future<void> main() async {
  KiwiContainer container = KiwiContainer();

  final FlutterI18nDelegate flutterI18nDelegate = FlutterI18nDelegate(
    translationLoader: FileTranslationLoader(
        decodeStrategies: [YamlDecodeStrategy()],
        useCountryCode: false,
        fallbackFile: 'en',
        basePath: 'assets/i18n',
        forcedLocale: Locale('fr')),
  );
  WidgetsFlutterBinding.ensureInitialized();

  await flutterI18nDelegate.load(null);

  container.registerSingleton((d) => flutterI18nDelegate);

  SystemChannels.textInput.invokeMethod('TextInput.hide');
  // await Sqflite.devSetDebugModeOn(true);
  // await Sqflite.setDebugModeOn(true);
  final String dbPath = 'benice_database.db';
  await ((await openDatabase(dbPath)).close());
  await deleteDatabase(dbPath);

  final database =
      await $FloorAppDatabase.databaseBuilder('benice_database.db').build();

  container.registerSingleton((d) => database.taskDao);
  container.registerSingleton((d) => database.consequenceDao);
  container.registerSingleton((d) => database.rewardDao);
  container.registerSingleton((d) => database.familyDao);
  container.registerSingleton((c) => ConsequencesBloc());
  container.registerSingleton((c) => FamilyBloc());

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp();
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          Provider<ConsequencesBloc>(
              create: (_) => KiwiContainer().resolve<ConsequencesBloc>()),
          Provider(create: (_) => RewardsBloc()),
          Provider(create: (_) => TaskBloc()),
        ],
        child: MaterialApp(
            localizationsDelegates: [
              KiwiContainer().resolve<FlutterI18nDelegate>(),
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              // GlobalCupertinoLocalizations.delegate,
            ],
            supportedLocales: [
              const Locale('en'), // English
              const Locale('fr'), // French
              const Locale('pt'), // Brazilian Portuguese
            ],
            title: 'BeNice',
            theme: ThemeData(
              // This is the theme of your application.
              //
              // Try running your application with "flutter run". You'll see the
              // application has a blue toolbar. Then, without quitting the app, try
              // changing the primarySwatch below to Colors.green and then invoke
              // "hot reload" (press "r" in the console where you ran "flutter run",
              // or simply save your changes to "hot reload" in a Flutter IDE).
              // Notice that the counter didn't reset back to zero; the application
              // is not restarted.
              primarySwatch: Colors.blue,
            ),
            home: new Scaffold(
                body: new GestureDetector(
                    onTap: () {
                      FocusScope.of(context).requestFocus(new FocusNode());
                    },
                    child: MyHomePage(title: 'Homepage')))));
  }
}

import 'dart:async';
import 'package:async/async.dart';

import 'package:benice_app/dao/consequence_dao.dart';
import 'package:benice_app/models/consequence.dart';
import 'package:kiwi/kiwi.dart';
import 'package:rxdart/rxdart.dart';

class ConsequencesBloc
// extends ChangeNotifier
{
  String _token = "%";
  ConsequenceDao consequenceDao = KiwiContainer().resolve<ConsequenceDao>();
  Stream<List<Consequence>> get consequences => _consequencesSearchSubject.stream;
  StreamSink<Consequence> get updateConsequence => _updateConsequence.sink;
  StreamSink<Consequence> get delete => _deleteConsequence.sink;
  StreamSink<String> get searchToken => _searchTokenSubject.sink;

  final _consequencesSearchSubject = BehaviorSubject<List<Consequence>>();
  final _consequencesSearchStream = StreamGroup<List<Consequence>>();


  final _updateConsequence = BehaviorSubject<Consequence>();
  final _deleteConsequence = BehaviorSubject<Consequence>();
  final _searchTokenSubject = BehaviorSubject<String>();

  ConsequencesBloc() {
    _init();
    _updateConsequence.stream.listen(_createConsequence);
    _deleteConsequence.stream.listen(_delete);
    _searchTokenSubject.stream.listen(_searchToken);
  }

  _init() async {
    _consequencesSearchStream.add(consequenceDao.findConsequences(_token));
    _consequencesSearchSubject.addStream(_consequencesSearchStream.stream);
  }

  _createConsequence(Consequence consequence) async {
    if (consequence.id != null)
      await consequenceDao.updateConsequence(consequence);
    else
      await consequenceDao.insertConsequence(consequence);
  }

  _delete(Consequence consequence) async {
    await consequenceDao.deleteConsequence(consequence);
  }

  _searchToken(String token) async {
    token.trim() != "" ? _token = '%$token%' : _token = "%";
    await _consequencesSearchStream.add(consequenceDao.findConsequences(_token));
  }

  dispose() {
    _updateConsequence.close();
    _consequencesSearchSubject.close();
    _consequencesSearchStream.close();
    _deleteConsequence.close();
    _searchTokenSubject.close();
  }
}

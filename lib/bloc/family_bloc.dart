import 'dart:async';

import 'package:benice_app/dao/family_dao.dart';
import 'package:benice_app/dao/user_dao.dart';
import 'package:benice_app/models/family.dart';
import 'package:benice_app/models/user.dart';
import 'package:kiwi/kiwi.dart';
import 'package:rxdart/rxdart.dart';

class FamilyBloc {
  FamilyDao familyDao = KiwiContainer().resolve<FamilyDao>();
  UserDao userDao = KiwiContainer().resolve<UserDao>();
  Stream<Family> get family => familyDao.findFamily();
  StreamSink<Family> get updateFamily => _updateFamilySubject.sink;

  final _updateFamilySubject = BehaviorSubject<Family>();

  FamilyBloc() {
    _updateFamilySubject.stream.listen(_updateFamily);
    _init();
  }

  _init() async {
    Family family = await familyDao.findFamilyFuture();
    if (family == null) {
      familyDao.insertFamily(Family.named(id: 1, name: "My Family"));
      userDao.insertUser(User.named(id:1, name: "Myself"));
    }
  }

  _updateFamily(Family family) async {
    if (family.id != null) {
      await familyDao.updateFamily(family);
    }
  }

  dispose() {
    _updateFamilySubject.close();
  }
}

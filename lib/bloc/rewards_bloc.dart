import 'dart:async';

import 'package:benice_app/dao/reward_dao.dart';
import 'package:benice_app/models/reward.dart';
import 'package:kiwi/kiwi.dart';
import 'package:rxdart/rxdart.dart';

class RewardsBloc {
  RewardDao rewardDao = KiwiContainer().resolve<RewardDao>();
  Stream<List<Reward>> get rewards => rewardDao.findAllRewardsAsStream();
  StreamSink<Reward> get updateReward => _updateReward.sink;
  StreamSink<Reward> get delete => _delete.sink;

  final _updateReward = BehaviorSubject<Reward>();
  final _delete = BehaviorSubject<Reward>();

  RewardsBloc() {
    _updateReward.stream.listen(_createUpdateReward);
    _delete.stream.listen(_deleteReward);
  }

  _createUpdateReward(Reward reward) async {
    if (reward.id != null) {
      await rewardDao.updateReward(reward);
    } else {
      await rewardDao.insertReward(reward);
    }
  }

  _deleteReward(Reward reward) async {
    await rewardDao.deleteReward(reward);
  }

  dispose() {
    _updateReward.close();
    _delete.close();
  }
}

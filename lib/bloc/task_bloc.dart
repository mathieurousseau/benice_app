import 'package:benice_app/dao/task_dao.dart';
import 'package:benice_app/models/task.dart';
import 'package:kiwi/kiwi.dart';
import 'dart:async';
import 'package:async/async.dart';

import 'package:rxdart/rxdart.dart';



class TaskBloc {
  TaskDao taskDao = KiwiContainer().resolve<TaskDao>();

  String _token = "%";
  Stream<List<Task>> get tasks => _tasksSubject.stream;
  StreamSink<Task> get save => _saveSubject.sink;
  StreamSink<Task> get delete => _deleteSubject.sink;
  StreamSink<String> get token => _tokenSubject.sink;

  final _tasksSubject = BehaviorSubject<List<Task>>();
  final _tasksStreamGroup = StreamGroup<List<Task>>();


  final _saveSubject = BehaviorSubject<Task>();
  final _deleteSubject = BehaviorSubject<Task>();
  final _tokenSubject = BehaviorSubject<String>();
  
  TaskBloc() {
    _init();
    _saveSubject.stream.listen(_saveTask);
    _deleteSubject.stream.listen(_deleteTask);
    _tokenSubject.stream.listen(_searchToken);
  }

  _init() async {
    _tasksStreamGroup.add(taskDao.findTasks(_token));
    _tasksSubject.addStream(_tasksStreamGroup.stream);
  }


  void _saveTask(Task task) async {
    if (task.id != null) {
      await taskDao.updateTask(task);
    } else {
      await taskDao.insertTask(task);
    }
  }

  void _deleteTask(Task task) async {
    await taskDao.deleteTask(task);
  }

  _searchToken(String token) async {
    token.trim() != "" ? _token = '%$token%' : _token = "%";
    await _tasksStreamGroup.add(taskDao.findTasks(_token));
  }

  dispose() {
    _saveSubject.close();
    _deleteSubject.close();
    _tokenSubject.close();
    _tasksStreamGroup.close();
    _tasksSubject.close();
  }
}

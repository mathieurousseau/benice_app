# benice_app

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.



# benice_app

Install VSCode

Download flutter
put it in /opt/flutter
add flutter/bin to PATH

Download androis studio installer. Run it and go through standard option wizard

run flutter doctor

Start VS Code.
Invoke View > Command Palette….
Type “install”, and select Extensions: Install Extensions.
Type “flutter” in the extensions search field, select Flutter in the list, and click Install. This also installs the required Dart plugin.

//delete all .g.dart files
Generate code:
flutter packages pub run build_runner build --delete-conflicting-outputs
Then watch:
flutter packages pub run build_runner watch


Watch to generate code:

iOS Simulator
 open -a Simulator
flutter run

Androir Simulator
emulator -avd pixel2 -gpu auto

Linux watch problem:
https://code.visualstudio.com/docs/setup/linux#_visual-studio-code-is-unable-to-watch-for-file-changes-in-this-large-workspace-error-enospc